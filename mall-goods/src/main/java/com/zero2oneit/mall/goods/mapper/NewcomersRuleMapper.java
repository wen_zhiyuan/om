package com.zero2oneit.mall.goods.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.goods.NewcomersRule;
import com.zero2oneit.mall.common.query.goods.NewcomersRuleQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Tg
 * @create 2021-06-02
 * @description
 */
@Mapper
public interface NewcomersRuleMapper extends BaseMapper<NewcomersRule> {

    int selectTotal(NewcomersRuleQueryObject qo);

    List<Map<String, Object>> selectAll(NewcomersRuleQueryObject qo);
}
