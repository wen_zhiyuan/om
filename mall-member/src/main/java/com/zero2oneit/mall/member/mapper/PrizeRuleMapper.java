package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.PrizeRule;
import com.zero2oneit.mall.common.query.member.PrizeRuleQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

/**
 * @author Tg
 * @create 2021-05-24
 * @description
 */
@Mapper
public interface PrizeRuleMapper extends BaseMapper<PrizeRule> {

    int selectTotal(PrizeRuleQueryObject qo);

    List<HashMap<String,Object>> selectRows(PrizeRuleQueryObject qo);

}
