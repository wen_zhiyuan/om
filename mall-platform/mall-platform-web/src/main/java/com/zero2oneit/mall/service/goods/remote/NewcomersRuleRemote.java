package com.zero2oneit.mall.service.goods.remote;

import com.zero2oneit.mall.common.bean.goods.NewcomersRule;
import com.zero2oneit.mall.common.query.goods.NewcomersRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.goods.NewcomersRuleFeign;
import com.zero2oneit.mall.system.base.utils.UserContext;
import com.zero2oneit.mall.system.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Description: 远程调用商品规格服务
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/2/2
 */
@RestController
@RequestMapping("/remote/newcomers")
@CrossOrigin
public class NewcomersRuleRemote {

    @Autowired
    private NewcomersRuleFeign ruleFeign;

    /**
     * 查询新人专享规则列表
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody NewcomersRuleQueryObject qo){
        return ruleFeign.list(qo);
    }

    /**
     * 添加或编辑新人专享规则信息
     * @param newcomersRule
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(NewcomersRule newcomersRule){
        User user = UserContext.getCurrentUser(UserContext.SYS_USER);
        newcomersRule.setOperatorName(user.getUsername());
        return ruleFeign.addOrEdit(newcomersRule);
    }

    /**
     * 删除新人专享规则信息
     * @param ids
     * @return
     */
    @PostMapping("/delByIds")
    public R delByIds(String ids){
        return ruleFeign.delByIds(ids);
    }

    /**
     * 更改新人专享信息状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/status")
    public R status(@RequestParam("id") String id, @RequestParam("status") Integer status){
        return ruleFeign.status(id, status);
    }

}
